const mongoose = require('mongoose')
const { Schema } = mongoose
const buildind = new Schema({
  name: { type: String, unique: true },
  level: Number

})
module.exports = mongoose.model('Building', buildind)
